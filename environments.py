from jinja2 import Environment

def latex_jinja_environment(environment):
	if not isinstance(environment, Environment):
		raise TypeError
	environment.block_start_string = '((*'
	environment.block_stop_string = '*))'
	environment.variable_start_string = '((('
	environment.variable_end_string = ')))'
	environment.comment_start_string = '((='
	environment.comment_end_string = '=))'


